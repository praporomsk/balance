


-- для теста
--chartboostAppId = "4f21c409cd1cb2fb7000001b"
--chartboostSignature = "92e2de2fd7070327bdeb54c15a5295309c6fcd2d"
-- Наш id
chartboostAppId = "54b2a5dc0d6025614a0e6bac"
chartboostSignature = "d42016428dc08a7fba9c1c165296b4af12341ab0"
adMobId = "ca-app-pub-1547231262595709/5306831279"
showChartboost = 1;
showInGameOver = 5;

game_url = "https://itunes.apple.com/app/id956742271"
share_text = "I scored %d in Sheep Hero! Beat that!"
facebook_page = "https://www.facebook.com/sheepherogame"
notificationText = "Come back! Sheep need their Hero!"

main_leaderboard = "com.darchenkov.sheephero.score"

showDebugMenu = 1;

gunCooldown = 0.1;
jetCooldown = 3;

sheep_speed = 300;

paths = {
  path1 = {
  	p1 = {x = 0,y = 510},
  	p2 = {x = 330,y = 510, air = 1},
	p3 = {x = 413,y = 510},
    p4 = {x = 493,y = 510},
    p5 = {x = 638,y = 510, air = 1},
	p6 = {x = 740,y = 510},
	p7 = {x = 1136,y = 384},
  	},
  path2 = {
  	p1 = {x = 0,y = 420},
  	p2 = {x = 271,y = 420, air = 1},
	p3 = {x = 370,y = 420},
    p4 = {x = 450,y = 420},
    p5 = {x = 663,y = 420, air = 1},
	p6 = {x = 763,y = 420},
	p7 = {x = 1136,y = 384},
  	},
  path3 = {
  	p1 = {x = 0,y = 320},
  	p2 = {x = 311,y = 320, air = 1},
	p3 = {x = 393,y = 320},
    p4 = {x = 473,y = 320},
    p5 = {x = 643,y = 320, air = 1},
	p6 = {x = 743,y = 320},
	p7 = {x = 1136,y = 384},
  	},
  path4 = {
  	p1 = {x = 0,y = 210},
  	p2 = {x = 262,y = 210, air = 1},
	p3 = {x = 359,y = 210},
    p4 = {x = 439,y = 210},
    p5 = {x = 661,y = 210, air = 1},
	p6 = {x = 756,y = 210},
	p7 = {x = 1135,y = 384},
  	},

}

waves = {
    -- EASY
	wave0  = {from = 0, to = 2, part=6, updateFactor=0.8},
	wave1  = {from = 0, to = 2, part=5, exce={0}, updateFactor=0.8},
	wave2  = {from = 0, to = 2, part=6, exce={0,1}, updateFactor=0.8},
    wave3  = {from = 1, to = 2, part=6, exce={2}, wolf = 1, updateFactor=0.8},
	wave4  = {from = 0, to = 3, part=6, exce={0,1,2},updateFactor=0.8},
	wave5  = {from = 0, to = 0, part=6, updateFactor=0.8},
    wave6  = {from = 2, to = 2, part=5, updateFactor=0.8},
	wave7  = {from = 1, to = 1, part=6, updateFactor=0.8},
	wave8  = {from = 3, to = 3, part=5, updateFactor=0.9},
	wave9  = {from = 0, to = 0, part=6, updateFactor=0.9},
	wave10 = {from = 2, to = 2, part=6, wolf = 1},
	-- NORMAL
	wave11 = {from = 0, to = 3, part=6, exce={9,10}},
	wave12 = {from = 0, to = 3, part=6, exce={11}},
	wave13 = {from = 0, to = 3, part=6, exce={12}, wolf = 1},
	wave14 = {from = 0, to = 3, part=4, exce={13}},
	wave15 = {from = 0, to = 3, part=5, exce={14}},
	wave16 = {from = 0, to = 3, part=6, exce={14,15}},
	wave17 = {from = 0, to = 3, part=6, exce={15,16}, wolf = 1},
	wave18 = {from = 0, to = 3, part=6, exce={17}},
	wave19 = {from = 0, to = 3, part=5, exce={17,18}},
    wave20 = {from = 0, to = 2, part=6, exce={19}, wolf = 1},
	wave21 = {from = 0, to = 2, part=6, exce={20}}, 
	wave22 = {from = 0, to = 2, part=4, exce={20,21}}, 
    wave23 = {from = 0, to = 3, part=4, wolf=1,exce={22}},
	wave24 = {from = 0, to = 3, part=6, exce={22,23}},
	wave25 = {from = 0, to = 0, part=4},
    wave26 = {from = 2, to = 2, part=6},
    wave27 = {from = 1, to = 1, part=4, wolf = 1},
	wave28 = {moveTo ={50,100,150,200,250,300}},
  -- FAST SHEEP
    wave50 = {from = 0, to = 3, part=5, exce={27},updateFactor=1.1},
    wave51 = {from = 0, to = 3, part=4, exce={50}, updateFactor=1.1},
    wave52 = {from = 0, to = 3, part=6, exce={50,51}, wolf = 1},
    wave53 = {from = 0, to = 3, part=6, exce={52}, updateFactor=1.2},
    wave54 = {from = 0, to = 2, part=6, updateFactor=1.1},
    wave55 = {from = 0, to = 2, part=6, exce={54},wolf = 1, updateFactor=1.2},
    wave56 = {from = 0, to = 2, part=6, exce={55}, updateFactor=1.1},
    wave57 = {from = 1, to = 3, part=6, exce={56},updateFactor=1.2},
    wave58 = {from = 1, to = 3, part=6, exce={57},updateFactor=1.1},
    wave59 =  {moveTo = {11}},
  -- PACK OF WOLVES
    wave100 = {from = 0, to = 0, part=6,  exce ={27}},
    wave101 = {from = 0, to = 2, part=5,   exce ={100}},
	wave102 = {from = 0, to = 2, time=1,   wolf = 1, exce={101}, updateFactor=1.2}, 
	wave103 = {from = 0, to = 2, time=1,   wolf = 1, exce={101,102}, updateFactor=1.2}, 
    wave104 = {from = 1, to = 3, time=1,   wolf = 1, exce={103}, updateFactor=1.2},
	wave105 = {from = 0, to = 3, time=0.98,wolf = 1, exce={103,104},updateFactor=1.3},
	wave106 = {from = 0, to = 0, time=1,   wolf = 1, updateFactor=1.2},
    wave107 = {from = 2, to = 2, time=0.9, wolf = 1, updateFactor=1.2},
    wave108 = {from = 1, to = 1, time=1,   wolf = 1, updateFactor=1.2},
	wave109 = {from = 3, to = 3, time=1,   wolf = 1, updateFactor=1.2},
    wave110 = {from = 0, to = 0, time=2,   part=6 , wolf = 1, updateFactor=1.2},
    wave111 = {moveTo = {11}},
	-- FAST SHEEP 2
    wave150 = {from = 1, to = 3, part=4, updateFactor=1.3},
    wave151 = {from = 0, to = 3, part=4, exce={49}, updateFactor=1.3},
    wave152 = {from = 0, to = 3, part=6, exce={49,50}, wolf = 1},
    wave153 = {from = 0, to = 3, part=6, exce={51}, updateFactor=1.3},
    wave154 = {from = 0, to = 2, part=6, exce={52}, updateFactor=1.3},
    wave155 = {from = 1, to = 2, part=6, exce={53}, wolf = 1, updateFactor=1.3},
    wave156 = {from = 0, to = 0, part=6, updateFactor=1.3},
    wave157 = {from = 1, to = 1, part=6, updateFactor=1.3},
    wave158 = {from = 2, to = 2, part=6, updateFactor=1.3},
    wave159 = {moveTo = {11}},
   -- PACK OF WOLVES 2
    wave200 = {from = 0, to = 1, time=0.7 ,wolf = 1, exce={27},  updateFactor=1.2}, 
	wave201 = {from = 0, to = 2, time=0.7, wolf = 1, exce={200}, updateFactor=1.2}, 
	wave202 = {from = 1, to = 3, time=0.7, wolf = 1, exce={201}, updateFactor=1.2}, 
    wave203 = {from = 0, to = 2, time=0.9, wolf = 1, exce={202}, updateFactor=1.2},
	wave204 = {from = 3, to = 3, time=0.7, wolf = 1, updateFactor=1.3},
	wave205 = {from = 0, to = 0, time=0.9, wolf = 1, updateFactor=1.2},
    wave206 = {from = 2, to = 2, time=0.9, wolf = 1, updateFactor=1.2},
    wave207 = {from = 1, to = 1, time=1,   wolf = 1, updateFactor=1.2},
	wave208 = {from = 3, to = 3, time=0.7, wolf = 1, updateFactor=1.2},
    wave209 = {from = 0, to = 0, time=0.9, wolf = 1, updateFactor=1.2},
    wave210 = {moveTo = {11}},

    --WOLF-SHEEP
    wave250 = {from = 1, to = 3, time=1,  exce={27}, wolf = 1}, 
	wave251 = {from = 0, to = 3, part=5,  exce={250}},
	wave252 = {from = 0, to = 3, time=1.1,exce={251}, wolf = 1},
	wave253 = {from = 1, to = 3, part=4,  exce={251,252}},
	wave254 = {from = 0, to = 0, time=1.1,wolf = 1},
	wave255 = {from = 2, to = 2, part=5}, 
	wave256 = {from = 3, to = 3, time=1.1,wolf = 1},
	wave257 = {from = 0, to = 0, part=5}, 
	wave258 = {from = 2, to = 2, time=1,  wolf = 1},
    wave259 = {from = 0, to = 3, time=1,  exce={258}, wolf = 1},
	wave260 = {from = 0, to = 2, part=7,  exce={259}}, 
	wave261 = {moveTo = {11}},

   --WOLF-SHEEP 2
    wave300 = {from = 0, to = 3, time=3, exce={27}},
    wave301 = {from = 0, to = 3, time=1.4, exce={300}, wolf = 1},
    wave302 = {from = 0, to = 3, part=4,exce={300,301}},
    wave303 = {from = 0, to = 3, part=4,exce={301,302}},
    wave304 = {from = 0, to = 3, time=0.7, exce={303}, wolf = 1},
    wave305 = {from = 0, to = 3, part=6, exce={303,304}},
    wave306 = {from = 0, to = 0, time=0.7,wolf = 1},
	wave307 = {from = 2, to = 2, part=5}, 
	wave308 = {from = 3, to = 3, time=0.9, wolf = 1},
	wave309 = {from = 0, to = 0, time=1.2}, 
	wave310 = {from = 2, to = 2, time=1, wolf = 1},
    wave311 = {from = 0, to = 3, time=1, exce={310},wolf = 1},
    wave312 = {moveTo={11},time=1.5},
}